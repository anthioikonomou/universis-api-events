import {parseExpression} from 'cron-parser';
import moment from 'moment';
/**
 * @param {DataEventArgs} event
 * @param {Function} callback
 */
export function beforeSave(event, callback) {
    return callback();
}

/**
 * @param {DataEventArgs} event
 */
async function afterSaveAsync(event) {
    if(event.target.hasOwnProperty('superEvent') && event.target.superEvent) {
        return;
    }
    if (event.state === 1) {
        if (!event.target.hasOwnProperty('eventHoursSpecification') || event.target.eventHoursSpecification === null) {
            return;
        }
        //get target event hours specification
        let thisEventHoursSpecification = typeof event.target.eventHoursSpecification !== 'object' ?
            await event.model.context.model('EventHoursSpecification')
                .where('id').equal(event.target.eventHoursSpecification)
                .getTypedItem() :
            event.model.context.model('EventHoursSpecification').convert(event.target.eventHoursSpecification);
        //fetch original data object
        const eventHoursSpecification = await event.model.context.model('EventHoursSpecification').where('id').equal(thisEventHoursSpecification.getId())
            .silent().getTypedItem();
        if (eventHoursSpecification) {
            //generate events
            let cronjob = eventHoursSpecification.toCronJobString();
            //get intervals
            let options = {
                currentDate: eventHoursSpecification.validFrom,
                endDate: eventHoursSpecification.validThrough,
                iterator: true
            };
            let interval = parseExpression(cronjob, options);
            let intervals = [];
            while (interval) {
                try {
                    let obj = interval.next();
                    intervals.push(obj.value);
                } catch (err) {
                    break;
                }
            }
            // const eventId = event.target.id;
            const {id , ..._event} = event.target;
            let events = intervals.map((x) => {
                return {
                    ..._event,
                    startDate: x.toDate(),
                    endDate: moment(x.toDate()).add(moment.duration(eventHoursSpecification.duration)).toDate(),
                    duration: eventHoursSpecification.duration,
                    superEvent: id,
                }
            });
            await event.model.context.model(event.target.additionalType).silent().save(events);
        }
    }
}

/**
 * @param {DataEventArgs} event
 * @param {Function} callback
 */
export function afterSave(event, callback) {
    // execute async method
    return afterSaveAsync(event).then(() => {
        return callback();
    }).catch(err => {
        return callback(err);
    });
}
