import {EdmMapping} from '@themost/data/odata';
import {DataObject} from '@themost/data/data-object';

/**
 * @class
 
 * @property {string} availableLanguage
 * @property {string} telephone
 * @property {string} email
 * @property {string} contactType
 * @property {string} faxNumber
 * @augments {DataObject}
 */
@EdmMapping.entityType('ContactPoint')
class ContactPoint extends DataObject {
    /**
     * @constructor
     */
    constructor() {
        super();
    }
}
module.exports = ContactPoint;
