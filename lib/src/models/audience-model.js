import {EdmMapping} from '@themost/data/odata';
import {DataObject} from '@themost/data/data-object';

/**
 * @class
 
 * @property {string} audienceType
 * @augments {DataObject}
 */
@EdmMapping.entityType('Audience')
class Audience extends DataObject {
    /**
     * @constructor
     */
    constructor() {
        super();
    }
}
module.exports = Audience;
